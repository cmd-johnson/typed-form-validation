import type { Result } from 'neverthrow'

import type { SchemaValidationError } from './schema-validation-error'

export interface SchemaValidator<
  T extends InputType,
  ErrorT extends SchemaValidationError,
  InputType = any
> {
  isSemanticallyValid: (value: T) => Result<T, ErrorT[]>
  isType(value: InputType): value is T
  assertIsType(value: InputType): asserts value is T
  asType(value: InputType): T
  validate(v: InputType): Result<T, ErrorT[]>
}

export function createSchemaValidator<
  T,
  ErrorT extends SchemaValidationError = SchemaValidationError
>(validate: (value: T) => Result<T, ErrorT[]>): SchemaValidator<T, ErrorT, T> {
  return {
    isSemanticallyValid: validate,
    isType: (value: T): value is T => true,
    assertIsType() {},
    asType(value: T) {
      return value
    },
    validate,
  }
}

export type SchemaValidatorErrors<T extends SchemaValidator<any, any, any>> =
  T extends SchemaValidator<any, infer ErrorT, any> ? ErrorT : never
