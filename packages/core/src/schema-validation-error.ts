import type { SchemaValidator } from './schema-validator'

export type FormPathKey = string | number

export type FormPath = ReadonlyArray<FormPathKey>

export type SchemaValidationError<
  Source extends string = string,
  Variants = object
> = {
  path: FormPath
  source: Source
} & Variants

export type SchemaValidatorError<Validator extends SchemaValidator<any, any>> =
  Validator extends SchemaValidator<any, infer ErrorT> ? ErrorT : never
