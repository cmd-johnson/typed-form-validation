import type { Static, TSchema } from '@sinclair/typebox'
import { Value } from '@sinclair/typebox/value'
import type {
  FormDataDeserializer,
  Json,
  SchemaValidationError,
  TypeMismatchError,
} from '@typed-form-validation/core'
import { createFormDataDeserializer } from '@typed-form-validation/core'
import cloneDeep from 'lodash/cloneDeep'
import deepMerge from 'lodash/merge'

import type { TypeboxSchemaValidator } from './schema-validator'

type ArrayDefaults<T> = Array<any> extends T
  ? T
  : T extends Record<string, any>
  ? {
      [K in keyof T as undefined extends T[K]
        ? never
        : Array<any> extends T[K]
        ? K
        : never]-?: ArrayDefaults<T[K]>
    }
  : never

function mergeArrayDefaults(
  arrayDefaults: ArrayDefaults<any>,
  data: Json
): Json {
  return deepMerge(cloneDeep(arrayDefaults), data)
}

export function createTypeboxFormDataDeserializer<
  Schema extends TSchema,
  ErrorT extends SchemaValidationError | never
>(
  typeboxValidator: TypeboxSchemaValidator<Schema, ErrorT>,
  defaults: ArrayDefaults<Static<Schema>> & Partial<Static<Schema>>
): FormDataDeserializer<Static<Schema>, ErrorT[] | TypeMismatchError> {
  return createFormDataDeserializer((data) => {
    const applyArrayDefaults = mergeArrayDefaults(defaults, data)
    const converted = Value.Convert(typeboxValidator.schema, applyArrayDefaults)

    return typeboxValidator.validate(converted)
  })
}
