export * from '@typed-form-validation/core'

export type {
  FormDataError,
  FormDataPath,
  FormDataPathValue,
  PathSegmentKey,
} from './form-data-path'
export { emptyArrayInputName } from './form-data-path'

export { FormArrayWrapper } from './form-array-wrapper'
export type { FormArrayWrapperProps } from './form-array-wrapper'

export { DiscriminatedUnionSwitch } from './form-discriminated-union-switch'
export type { DiscriminatedUnionSwitchProps } from './form-discriminated-union-switch'

export { FormDiscriminatedUnionWrapper } from './form-discriminated-union-wrapper'
export type { FormDiscriminatedUnionWrapperProps } from './form-discriminated-union-wrapper'

export { FormInputWrapper } from './form-input-wrapper'
export type {
  FormInputWrapperChildProps,
  FormInputWrapperProps,
} from './form-input-wrapper'

export {
  useFormArrayItems,
  useIsFormArrayEmpty,
  useWriteonlyFormArray,
} from './hooks/use-form-array'

export { useFormValue, useSetFormValue } from './hooks/use-form-value'
export type { FormValueSetter } from './hooks/use-form-value'

export { useForm } from './hooks/use-form'

export { useGetFormData } from './hooks/use-get-form-data'

export { useHasChanges } from './hooks/use-has-changes'

export { useIsFormValid } from './hooks/use-is-form-valid'
