import type { SchemaValidationError } from '@typed-form-validation/core'
import React from 'react'

import type { FormDataPath } from './form-data-path'
import { useFormArrayItems } from './hooks/use-form-array'

export interface FormArrayWrapperProps<
  ItemT,
  ErrorT extends SchemaValidationError
> {
  path: FormDataPath<ItemT[], ErrorT>
  getKey?: (item: ItemT) => React.Key
  whenEmpty?: React.ReactNode
  children: (
    props: ReturnType<typeof useFormArrayItems<ItemT, ErrorT>>
  ) => React.ReactNode | React.ReactNode[]
}
export function FormArrayWrapper<ItemT, ErrorT extends SchemaValidationError>({
  path,
  getKey,
  whenEmpty,
  children,
}: FormArrayWrapperProps<ItemT, ErrorT>): JSX.Element {
  const items = useFormArrayItems(path, getKey)
  if (items.length < 1 && whenEmpty) {
    return <>{whenEmpty}</>
  }
  return <>{children(items)}</>
}
