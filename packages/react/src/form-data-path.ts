import type {
  FormPath,
  FormPathKey,
  SchemaValidationError,
} from '@typed-form-validation/core'

import type { FormDataInternals } from './internals'
import { _internals } from './internals'

export type PathSegmentKey<T> = keyof T & FormPathKey

export type FormDataPathValue<Path extends FormDataPath<any, any>> =
  Path extends FormDataPath<infer T, any> ? T : never

export type FormDataError<Path extends FormDataPath<any, any>> =
  Path extends FormDataPath<any, infer ErrorT> ? ErrorT : never

declare const dataType: unique symbol
declare const errorType: unique symbol

export interface FormDataPath<T, ErrorT extends SchemaValidationError = any> {
  [dataType]: T
  [errorType]: ErrorT

  [_internals]: FormDataInternals<ErrorT>

  name: string
  path: FormPath

  at<Path extends PathSegmentKey<T>>(key: Path): FormDataPath<T[Path], ErrorT>
}

export function createFormDataPath<
  T,
  ErrorT extends SchemaValidationError = any
>(
  path: FormPath,
  internals: FormDataInternals<ErrorT>
): FormDataPath<T, ErrorT> {
  const childCache = new Map<
    keyof T & FormPathKey,
    FormDataPath<unknown, ErrorT>
  >()

  const tmp: Omit<
    FormDataPath<T, ErrorT>,
    typeof dataType | typeof errorType
  > = {
    [_internals]: internals,
    name: internals.getNameFromPath(path),
    path,
    at: <Path extends PathSegmentKey<T>>(key: Path) => {
      const child =
        childCache.get(key) ??
        (() => {
          const child = createFormDataPath<T[Path], ErrorT>(
            path.concat(key),
            internals
          )
          childCache.set(key, child)
          return child
        })()
      return child as unknown as FormDataPath<T[Path], ErrorT>
    },
  }

  // dataType and errorType properties only exist on the type level to allow for
  // strict type checking
  return tmp as FormDataPath<T, ErrorT>
}

export function isFormDataPath<T extends FormDataPath<any, any>>(
  value: unknown
): value is Extract<T, FormDataPath<any, any>> {
  return typeof value === 'object' && !!value && _internals in value
}

export function emptyArrayInputName(path: FormDataPath<any[]>): string {
  return (
    path[_internals].getNameFromPath(path.path, {
      skipScalarArraySuffix: true,
    }) + '[_]'
  )
}
