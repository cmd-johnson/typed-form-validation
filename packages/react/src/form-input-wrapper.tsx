import React from 'react'

import type {
  FormDataError,
  FormDataPath,
  FormDataPathValue,
} from './form-data-path'
import type { FormValueSetter } from './hooks/use-form-value'
import { useFormValue } from './hooks/use-form-value'

export interface FormInputWrapperChildProps<
  Path extends FormDataPath<any, any>
> {
  name: string
  value: FormDataPathValue<Path>
  onChange: FormValueSetter<FormDataPathValue<Path>>
  errorText: string | null
  errors: FormDataError<Path>[]
}

export interface FormInputWrapperProps<Path extends FormDataPath<any, any>> {
  path: Path
  children: (
    props: FormInputWrapperChildProps<Path>
  ) => React.ReactNode | React.ReactNode[]
}
export function FormInputWrapper<Path extends FormDataPath<any, any>>({
  path,
  children,
}: FormInputWrapperProps<Path>): JSX.Element {
  const [value, onChange, errorText, errors] = useFormValue(path)

  return (
    <>{children({ name: path.name, value, onChange, errorText, errors })}</>
  )
}
