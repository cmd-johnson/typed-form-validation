import type {
  FormPath,
  SchemaValidationError,
} from '@typed-form-validation/core'

import type { FormDataPath } from './form-data-path'

export const _internals = Symbol('internals')
export interface FormDataInternals<
  ErrorT extends SchemaValidationError = SchemaValidationError
> {
  getValueAt: <T>(path: FormDataPath<T, ErrorT>) => { value: T } | null
  getNameFromPath: (
    path: FormPath,
    options?: { skipScalarArraySuffix?: boolean }
  ) => string
  getErrorsAt: (path: FormDataPath<any, ErrorT>) => ErrorT[]
  update: <T>(path: FormPath, newValue: T) => void
  subscribe: <T>(
    path: FormDataPath<T, ErrorT> | FormPath,
    notify: () => void
  ) => () => void
  formatErrors: (errors: ErrorT[]) => string | null
}
