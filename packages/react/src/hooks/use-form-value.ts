import { useCallback, useMemo, useRef } from 'react'

import type {
  FormDataError,
  FormDataPath,
  FormDataPathValue,
} from '../form-data-path'
import { _internals } from '../internals'
import { useForceUpdate } from './use-force-update'
import { useFormDataSubscription } from './use-form-data-subscription'

export type AnyFunction = (...args: any[]) => any

export interface FormValueSetter<Value> {
  (newValue: Value): void
  (transform: (oldValue: { value: Value } | null) => Value): void
}

export function useFormValue<Path extends FormDataPath<any, any>>(
  path: Path
): [
  value: FormDataPathValue<Path>,
  setValue: FormValueSetter<FormDataPathValue<Path>>,
  errorText: string | null,
  errors: FormDataError<Path>[]
] {
  type ValueT = FormDataPathValue<Path>
  type ErrorT = FormDataError<Path>

  const { getValueAt, getErrorsAt, update, formatErrors } = path[_internals]

  const forceUpdate = useForceUpdate()

  const valueRef = useRef<{ value: ValueT } | null>()
  const errorTextRef = useRef<string | null>()
  const errorsRef = useRef<ErrorT[]>()

  useMemo(() => {
    valueRef.current = getValueAt(path)

    const errors = getErrorsAt(path)
    errorTextRef.current = errors.length > 0 ? formatErrors(errors) : null
    errorsRef.current = errors
  }, [formatErrors, getErrorsAt, getValueAt, path])

  const handleChange = useCallback(() => {
    const newValue = getValueAt(path)

    const errors = getErrorsAt(path)
    const errorText = errors.length > 0 ? formatErrors(errors) : null

    if (
      newValue === valueRef.current &&
      errors.length === errorsRef.current?.length &&
      errorText === errorTextRef.current &&
      JSON.stringify(errors) === JSON.stringify(errorsRef.current) // TODO: find a cleaner way to test this
    ) {
      // looks like the value and errors haven't changed
      return
    }

    valueRef.current = newValue
    errorTextRef.current = errors.length > 0 ? formatErrors(errors) : null
    errorsRef.current = errors

    forceUpdate()
  }, [forceUpdate, formatErrors, getErrorsAt, getValueAt, path])

  useFormDataSubscription(path, handleChange)

  const setValue = useCallback<FormValueSetter<ValueT>>(
    (newValue) => {
      if (typeof newValue === 'function') {
        const getNewValue = newValue as (
          oldValue: { value: ValueT } | null
        ) => ValueT
        update(path.path, getNewValue(valueRef.current ?? null))
      } else {
        update(path.path, newValue)
      }
    },
    [path, update]
  )

  return [
    valueRef.current?.value as ValueT,
    setValue,
    errorTextRef.current ?? null,
    errorsRef.current as ErrorT[],
  ]
}

export function useSetFormValue<Path extends FormDataPath<any, any>>(
  path: Path
): FormValueSetter<FormDataPathValue<Path>> {
  type ValueT = FormDataPathValue<Path>

  const { update, getValueAt } = path[_internals]

  return useCallback<FormValueSetter<ValueT>>(
    (newValue) => {
      if (typeof newValue === 'function') {
        const getNewValue = newValue as (
          oldValue: { value: ValueT } | null
        ) => ValueT
        update(path.path, getNewValue(getValueAt(path)))
      } else {
        update(path.path, newValue as ValueT)
      }
    },
    [getValueAt, path, update]
  )
}
