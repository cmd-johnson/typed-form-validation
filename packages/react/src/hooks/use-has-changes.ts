import { useCallback, useMemo, useRef } from 'react'

import type { FormDataPath } from '../form-data-path'
import { _internals } from '../internals'
import { useForceUpdate } from './use-force-update'
import { useFormDataSubscription } from './use-form-data-subscription'

export function useHasChanges<T>(
  path: FormDataPath<T, any>,
  initialValue: T
): boolean {
  const hasChanges = useRef(false)

  const forceUpdate = useForceUpdate()

  const refreshValue = useCallback(() => {
    const value = path[_internals].getValueAt(path)
    const newHasChanges = value?.value !== initialValue
    if (hasChanges.current === newHasChanges) return
    hasChanges.current = newHasChanges
    forceUpdate()
  }, [forceUpdate, initialValue, path])

  // We only want to refresh if the initialValue changes
  // The other direction is handled by the useFormDataSubscription further down
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useMemo(() => refreshValue(), [initialValue])

  useFormDataSubscription(path, refreshValue)

  return hasChanges.current
}
