import type { SchemaValidationError } from '@typed-form-validation/core'
import { useCallback } from 'react'

import type { FormDataPath } from '../form-data-path'
import { _internals } from '../internals'

export function useGetFormData<
  T,
  ErrorT extends SchemaValidationError = SchemaValidationError
>(
  path: FormDataPath<T, ErrorT>
): () => { value: T } | { errors: ErrorT[]; errorText: string | null } {
  return useCallback(() => {
    const value = path[_internals].getValueAt(path)
    if (!value) {
      const errors = path[_internals].getErrorsAt(path)
      return { errors, errorText: path[_internals].formatErrors(errors) }
    }
    return value
  }, [path])
}
