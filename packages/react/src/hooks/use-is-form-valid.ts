import { useCallback, useMemo, useRef } from 'react'

import type { FormDataPath } from '../form-data-path'
import { _internals } from '../internals'
import { useForceUpdate } from './use-force-update'
import { useFormDataSubscription } from './use-form-data-subscription'

export function useIsFormValid<Path extends FormDataPath<any, any>>(
  path: Path
): boolean {
  const { getErrorsAt } = path[_internals]

  const isValidRef = useRef(false)

  useMemo(
    () => (isValidRef.current = getErrorsAt(path).length < 1),
    [getErrorsAt, path]
  )

  const forceUpdate = useForceUpdate()

  useFormDataSubscription(
    path,
    useCallback(() => {
      const isValid = getErrorsAt(path).length < 1
      if (isValid === isValidRef.current) return
      isValidRef.current = isValid
      forceUpdate()
    }, [forceUpdate, getErrorsAt, path])
  )

  return isValidRef.current
}
